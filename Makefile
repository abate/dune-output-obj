
all:
	dune build @install

clean:
	dune clean
	make -C tests clean

test: all
	dune build lib/cstub.so
	make -C tests
	tests/test

