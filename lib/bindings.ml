open Ctypes
(* open Foreign *)

type result = [`result] structure
let result : result typ = structure "result"

let ok (i : int) : result ptr =
  let j = Result.Ok i in
  Root.create j |> from_voidp result

module Stubs (I : Cstubs_inverted.INTERNAL) = struct

  let () = I.structure result

  let () = I.internal
    "ok" (int @-> returning (ptr result)) ok

end
